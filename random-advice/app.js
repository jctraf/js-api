const getAdvice = function() {
    alert("button clicked!");
    const adviceLabel = document.querySelector("#advice-label");
    const adviceNumberLabel = document.querySelector("#advice-number-label");
    adviceLabel.innerText = "you clicked the button!";
    adviceNumberLabel.innerText = "54";

    // 1. @TODO: Define the URL of the API
    
    // 2. @TODO: Make your API call and wait for response
    
    // 3. @TODO: Convert the response to a JSON object
  
    // 4. @TODO: Parse the object object for the information you want.

    // 5. @TODO: Do something with the information
    //      - Example: Display it in the UI
    //      - Example: Save the data to a database
    //      - Example: Do some type of calculation on the info
    
}

// event listeners
document.querySelector("#btn-yes").addEventListener("click", getAdvice);