//@TODO: Put your API key here
const API_KEY = ""

// List of possible cities
const CITIES = ["Chicago", "London", "Sydney", "Moscow", "New York"];

const getWeather = function() {
    console.log("getting the weather");
    const otherCityLabel = document.querySelector("#other-city-name");
    const otherCityTempLabel = document.querySelector("#other-city-temp");
    const torontoTemp = document.querySelector("#toronto-temp")

    // 1. @TODO: Define the URL of the API
    
    // 2. @TODO: Make your API call and wait for response
    
    // 3. @TODO: Convert the response to a JSON object
  
    // 4. @TODO: Parse the object object for the information you want.

    // 5. @TODO: Do something with the information
    //      - Example: Display it in the UI
    //      - Example: Save the data to a database
    //      - Example: Do some type of calculation on the info
    
}

const checkWinner = function() {
    console.log("Checking winner");
    // 1. Get temperature from both cities

    // 2. Calculate winner (higher temp wins)

    // 3. Display winner
}

// event listeners
document.querySelector("#btn-weather").addEventListener("click", getWeather);
document.querySelector("#btn-winner").addEventListener("click", checkWinner);